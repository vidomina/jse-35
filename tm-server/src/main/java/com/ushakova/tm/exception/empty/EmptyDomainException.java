package com.ushakova.tm.exception.empty;

import com.ushakova.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("An error has occurred: domain is empty.");
    }

}
