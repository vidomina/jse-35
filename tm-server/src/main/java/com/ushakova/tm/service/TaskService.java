package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.service.ITaskService;
import com.ushakova.tm.exception.empty.EmptyDescriptionException;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    @NotNull
    public Task add(@Nullable final String name, @Nullable final String description, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }

}
