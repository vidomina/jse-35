package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.IPropertyService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.empty.*;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository,
                       @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return userRepository.add(user);
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = add(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = add(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    @NotNull
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable User user = userRepository.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    @NotNull
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @Override
    @NotNull
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable User user = userRepository.removeByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Override
    @NotNull
    public User removeUser(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        userRepository.removeUser(user);
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyPasswordException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    @NotNull
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

    @Override
    @NotNull
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
