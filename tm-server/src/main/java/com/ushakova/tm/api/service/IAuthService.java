package com.ushakova.tm.api.service;

import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void checkRoles(@Nullable Role... roles);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@NotNull String login, @NotNull String password);

    void logout();

    void registry(@NotNull String login, @NotNull String password, @Nullable String email);

}
