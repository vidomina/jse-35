package com.ushakova.tm.api.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

}
