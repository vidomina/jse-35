package com.ushakova.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    Integer getBackupInterval();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getSessionIteration();

    @NotNull
    String getSessionSecret();

}
