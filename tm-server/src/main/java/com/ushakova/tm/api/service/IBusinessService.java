package com.ushakova.tm.api.service;

import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @Nullable
    E add(@Nullable String userId, @NotNull final E entity);

    void addAll(@Nullable String userId, @Nullable List<E> list);

    @NotNull
    E changeStatusById(@Nullable String id, @NotNull Status status, @Nullable String userId);

    @NotNull
    E changeStatusByIndex(@Nullable Integer index, @NotNull Status status, @Nullable String userId);

    @NotNull
    E changeStatusByName(@Nullable String name, @NotNull Status status, @Nullable String userId);

    void clear(@Nullable String userId);

    @NotNull
    E completeById(@Nullable String id, @Nullable String userId);

    @NotNull
    E completeByIndex(@Nullable Integer index, @Nullable String userId);

    @NotNull
    E completeByName(@Nullable String name, @Nullable String userId);

    @Nullable
    List<E> findAll(@Nullable Comparator<E> comparator, @Nullable String userId);

    @Nullable
    List<E> findAll(@Nullable String userId);

    @NotNull
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable Integer index, @Nullable String userId);

    @NotNull
    E findByName(@Nullable String name, @Nullable String userId);

    @Nullable
    E removeById(@Nullable String id, @Nullable String userId);

    @Nullable
    E removeByIndex(@Nullable Integer index, @Nullable String userId);

    @Nullable
    E removeByName(@Nullable String name, @Nullable String userId);

    @NotNull
    E startById(@Nullable String id, @Nullable String userId);

    @NotNull
    E startByIndex(@Nullable Integer index, @Nullable String userId);

    @NotNull
    E startByName(@Nullable String name, @Nullable String userId);

    @NotNull
    E updateById(@Nullable String id, @Nullable String name, @Nullable String description, @Nullable String userId);

    @NotNull
    E updateByIndex(@Nullable Integer index, @Nullable String name, @Nullable String description, @Nullable String userId);

}
