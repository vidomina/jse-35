package com.ushakova.tm.api.endpoint;

import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminDataEndpoint {

    @WebMethod
    void loadBackup(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataBase64(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataBin(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataJson(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataJsonJaxB(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataXml(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataXmlJaxB(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void loadDataYaml(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveBackup(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataBase64(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataBin(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataJson(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataJsonJaxB(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataXml(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataXmlJaxB(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void saveDataYaml(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

}
