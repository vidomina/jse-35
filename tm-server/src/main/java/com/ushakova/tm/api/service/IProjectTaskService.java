package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskByProject(@Nullable String projectId, @Nullable String taskId, @Nullable String userId);

    @Nullable
    List<Task> findAllTaskByProjectId(@Nullable String projectId, @Nullable String userId);

    @Nullable
    Project removeProjectById(@Nullable String projectId, @Nullable String userId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String taskId, @Nullable String userId);

}

