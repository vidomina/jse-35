package com.ushakova.tm.api.endpoint;

import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void addAllUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<User> entities
    );

    @WebMethod
    void addUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    void clearUser(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull User createUser(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User createUserWithEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    @NotNull User createUserWithRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    @NotNull List<User> findAllUser(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull User findUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable User findUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String login
    );

    @WebMethod
    @NotNull User lockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    );

    @WebMethod
    void removeUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    );

    @WebMethod
    @Nullable User removeUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull User removeUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    );

    @WebMethod
    void setPasswordUser(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User unlockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    );

    @WebMethod
    void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName
    );

}
