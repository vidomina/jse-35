package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IBusinessRepository;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    @NotNull
    public E add(@NotNull final E entity, @NotNull final String userId) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.removeAll(entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList()));
    }

    @Override
    @Nullable
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public List<E> findAll(@NotNull Comparator<E> comparator, @NotNull final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public E findById(@NotNull final String id, @NotNull final String userId) {
        @Nullable final List<E> list = findAll(userId);
        Optional.ofNullable(list).orElseThrow(EntityNotFoundException::new);
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E findByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @Nullable final E entity = entities.get(index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    @Nullable
    public E findByName(@NotNull final String name, @NotNull final String userId) {
        return entities.stream()
                .filter(e -> e.getName().equals(name) && userId.equals(e.getUserId()))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public void remove(@NotNull final E entity, @NotNull final String userId) {
        if (userId.equals(entity.getUserId())) {
            entities.remove(entity);
        }
    }

    @Override
    @Nullable
    public E removeById(@NotNull final String id, @NotNull final String userId) {
        @Nullable final E entity = findById(userId, id);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeByIndex(@NotNull final Integer index, @NotNull final String userId) {
        @Nullable final E entity = findByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeByName(@NotNull final String name, @NotNull final String userId) {
        @Nullable final E entity = findByName(name, userId);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

}
