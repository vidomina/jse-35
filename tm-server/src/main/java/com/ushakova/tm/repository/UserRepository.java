package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        return entities.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        return entities.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User removeByLogin(@Nullable final String login) {
        @Nullable final User user = findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        remove(user);
        return user;
    }

    @Override
    @Nullable
    public User removeUser(@Nullable final User user) {
        entities.remove(user);
        return user;
    }

}
