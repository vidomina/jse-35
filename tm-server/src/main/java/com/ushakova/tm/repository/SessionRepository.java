package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ISessionRepository;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull final String id) {
        return entities.stream().anyMatch(e -> id.equals(e.getId()));
    }

}

