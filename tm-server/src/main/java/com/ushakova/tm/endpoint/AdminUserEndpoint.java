package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.IAdminUserEndpoint;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(@NotNull final IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addAllUser(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "entities") final List<User> entities
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.addAll(entities);
    }

    @Override
    @WebMethod
    public void addUser(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "entity") final User entity
    ) {

        sessionService.validate(session, Role.ADMIN);
        userService.add(entity);
    }

    @Override
    @WebMethod
    public void clearUser(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    public @NotNull User createUser(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login,
            @Nullable @WebParam(name = "password") final String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(login, password);
    }

    @Override
    @WebMethod
    public @NotNull User createUserWithEmail(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login,
            @Nullable @WebParam(name = "password") final String password,
            @Nullable @WebParam(name = "email") final String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(login, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createUserWithRole(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login,
            @Nullable @WebParam(name = "password") final String password,
            @Nullable @WebParam(name = "role") final Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(login, password, role);
    }

    @Override
    @WebMethod
    public @NotNull List<User> findAllUser(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public @NotNull User findUserById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findById(id);
    }

    @Override
    @WebMethod
    public @Nullable User findUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @WebMethod
    public @NotNull User lockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUser(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "entity") final User entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.remove(entity);
    }

    @Override
    @WebMethod
    public @Nullable User removeUserById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeById(id);
    }

    @Override
    @WebMethod
    public @NotNull User removeUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void setPasswordUser(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "password") final String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.setPassword(id, password);
    }

    @Override
    @WebMethod
    public @NotNull User unlockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
