package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.IUserEndpoint;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
    ) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "password") final String password
    ) {
        sessionService.validate(session);
        userService.setPassword(session.getUserId(), password);
    }

}
