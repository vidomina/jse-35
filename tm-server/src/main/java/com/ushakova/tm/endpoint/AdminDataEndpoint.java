package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.IAdminDataEndpoint;
import com.ushakova.tm.api.service.IDataService;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.component.Backup;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    private Backup backup;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IDataService dataService;

    public AdminDataEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final Backup backup,
            @NotNull final IDataService dataService
    ) {
        this.sessionService = sessionService;
        this.backup = backup;
        this.dataService = dataService;
    }

    @Override
    @WebMethod
    public void loadBackup(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    public void loadDataBase64(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataBase64();
    }

    @Override
    @WebMethod
    public void loadDataBin(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataBin();
    }

    @Override
    @WebMethod
    public void loadDataJson(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataJson();
    }

    @Override
    @WebMethod
    public void loadDataJsonJaxB(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataJsonJaxB();
    }

    @Override
    @WebMethod
    public void loadDataXml(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataXml();
    }

    @Override
    @WebMethod
    public void loadDataXmlJaxB(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataXmlJaxB();
    }

    @Override
    @WebMethod
    public void loadDataYaml(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.loadDataYaml();
    }

    @Override
    @WebMethod
    public void saveBackup(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        backup.init();
    }

    @Override
    @WebMethod
    public void saveDataBase64(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataBase64();
    }

    @Override
    @WebMethod
    public void saveDataBin(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataBin();
    }

    @Override
    @WebMethod
    public void saveDataJson(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataJson();
    }

    @Override
    @WebMethod
    public void saveDataJsonJaxB(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataJsonJaxB();
    }

    @Override
    @WebMethod
    public void saveDataXml(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataXml();
    }

    @Override
    @WebMethod
    public void saveDataXmlJaxB(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataXmlJaxB();
    }

    @Override
    @WebMethod
    public void saveDataYaml(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session, Role.ADMIN);
        dataService.saveDataYaml();
    }

}
