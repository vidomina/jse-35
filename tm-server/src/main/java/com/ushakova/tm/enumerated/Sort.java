package com.ushakova.tm.enumerated;

import com.ushakova.tm.comparator.ComparatorByCreated;
import com.ushakova.tm.comparator.ComparatorByDateStart;
import com.ushakova.tm.comparator.ComparatorByName;
import com.ushakova.tm.comparator.ComparatorByStatus;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort By Created", ComparatorByCreated.getInstance()),
    DATE_START("Sort By Start", ComparatorByDateStart.getInstance()),
    NAME("Sort By Name", ComparatorByName.getInstance()),
    STATUS("Sort By Status", ComparatorByStatus.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
