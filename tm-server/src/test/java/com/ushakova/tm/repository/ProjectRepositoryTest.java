package com.ushakova.tm.repository;

import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectRepositoryTest {

    @Nullable
    private final String projectDescription = "Project Tests";
    @Nullable
    private ProjectRepository projectRepository;
    @Nullable
    private Project project;
    @Nullable
    private String userId;
    @NotNull
    private String projectName = "jse-35";
    @NotNull
    private String projectId;

    @After
    public void after() {
        projectRepository.clear(userId);
    }

    @Before
    public void before() {
        projectRepository = new ProjectRepository();
        @NotNull User user = new User();
        user.setLogin("Java");
        user.setPasswordHash(HashUtil.salt("Java", 2, "123paSS"));
        project = new Project();
        projectId = project.getId();
        project.setUserId(user.getId());
        project.setName(projectName);
        project.setDescription(projectDescription);
        projectRepository.add(project);
    }

    @Test
    public void findProject() throws AbstractException {
        Assert.assertEquals(project, projectRepository.findByName(projectName, userId));
        Assert.assertEquals(project, projectRepository.findById(projectId));
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(0, userId));
    }

    @Test
    public void removeByName() throws AbstractException {
        Assert.assertNotNull(project);
        projectRepository.removeByName(projectName, userId);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

}
