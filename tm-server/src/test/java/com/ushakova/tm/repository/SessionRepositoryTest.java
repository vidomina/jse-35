package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ISessionRepository;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionRepositoryTest {

    @NotNull
    private Session session;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private String sessionId;

    @After
    public void after() {
        sessionRepository.clear();
    }

    @Before
    public void before() {
        sessionRepository = new SessionRepository();
        session = new Session();
        sessionId = session.getId();
        session.setUserId(new User().getId());
    }

    @Test
    public void contains() {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @Test
    public void openClose() throws AbstractException {
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.findById(sessionId));
        sessionRepository.remove(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

}
