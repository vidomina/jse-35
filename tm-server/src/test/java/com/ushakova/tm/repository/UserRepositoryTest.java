package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userLogin = "userTest";

    public UserRepositoryTest() {
        userRepository = new UserRepository();
        user = new User();
        user.setLogin(userLogin);
    }

    @After
    public void finalizeTest() {
        userRepository.clear();
    }

    @Test
    public void findByEmail() throws AbstractException {
        Assert.assertEquals(user, userRepository.findByEmail(userLogin));
    }

    @Test
    public void findByLogin() throws AbstractException {
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
    }

    @Before
    public void initializeTest() throws AbstractException {
        userRepository.add(user);
    }

    @Test
    public void removeByLogin() throws AbstractException {
        userRepository.removeByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeUser() throws AbstractException {
        userRepository.removeUser(user);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

}
