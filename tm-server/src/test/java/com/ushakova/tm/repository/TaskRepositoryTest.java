package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @Nullable
    private Task task;

    @Nullable
    private String userId;

    @NotNull
    private String taskName = "jse-35";

    @Nullable
    private String taskDescription = "Task Tests";

    @NotNull
    private String taskId;

    @NotNull
    private Project project;

    @NotNull
    private String projectId;

    @After
    public void after() {
        taskRepository.clear(userId);
    }

    @Before
    public void before() {
        taskRepository = new TaskRepository();
        @NotNull User user = new User();
        user.setLogin("Java");
        user.setPasswordHash(HashUtil.salt("Java", 2, "123paSS"));
        task = new Task();
        taskId = task.getId();
        task.setUserId(user.getId());
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(task);
    }

    @Test
    public void findAllByProjectId(
    ) throws AbstractException {
        task.setProjectId(projectId);
        Assert.assertEquals(task, taskRepository.findAllByProjectId(projectId, userId).get(0));
    }

    @Test
    public void findtask() throws AbstractException {
        Assert.assertEquals(task, taskRepository.findByName(taskName, userId));
        Assert.assertEquals(task, taskRepository.findById(taskId));
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(0, userId));
    }

    @Test
    public void removeByName() throws AbstractException {
        Assert.assertNotNull(task);
        taskRepository.removeByName(taskName, userId);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

}
