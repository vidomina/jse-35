package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.IPropertyService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserServiceTest {

    @NotNull
    final private String userLogin = "login";
    @Nullable
    final private String userPassword = "password";
    @NotNull
    private IUserService userService;
    @NotNull
    private IPropertyService propertyService;
    @NotNull
    private User user;
    @NotNull
    private String userId;

    @After
    public void after() throws AbstractException {
        userService.clear();
    }

    @Before
    public void before() throws AbstractException {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setPasswordHash(HashUtil.salt("qwe", 3, userPassword));
        @NotNull final IUserRepository userRepository = new UserRepository();
        userRepository.add(user);
        propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserLogin";
        @NotNull final String newUserPassword = "newUserPassword";
        @NotNull final User newUser = userService.add(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final User tempUser = userService.findByLogin(userLogin);
        Assert.assertEquals(user, tempUser);
        Assert.assertEquals(userLogin, tempUser.getLogin());
        Assert.assertEquals(HashUtil.salt("qwe", 3, userPassword), tempUser.getPasswordHash());
    }

    @Test
    public void lockUnlockByLogin() throws AbstractException {
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.getLocked());
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(1, userService.findAll().size());
        userService.removeByLogin(userLogin);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertEquals(HashUtil.salt("qwe", 3, userPassword), user.getPasswordHash());
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(HashUtil.salt("qwe", 3, userPassword), user.getPasswordHash());
        int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPasswordHash());
    }

    @Test
    public void setRole() throws AbstractException {
        Assert.assertNotNull(user.getRole());
        Assert.assertEquals(user.getRole(), Role.USER);
        user.setRole(Role.ADMIN);
        Assert.assertEquals(user.getRole(), Role.ADMIN);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newLastName = "newLastName";
        @NotNull final String newFirstName = "newFirstName";
        @NotNull final String newMiddleName = "newMiddleName";
        @NotNull final String newEmail = "email";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
    }

}
