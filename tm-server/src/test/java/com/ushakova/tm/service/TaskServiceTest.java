package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.service.IProjectTaskService;
import com.ushakova.tm.api.service.ITaskService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TaskServiceTest {

    @NotNull
    private final String taskName = "taskName";
    @NotNull
    private final String taskDescription = "taskDescription";
    @NotNull
    private final String userId = "userId";
    @NotNull
    private ITaskService taskService;
    @NotNull
    private IProjectTaskService projectTaskService;
    @NotNull
    private Task task;
    @NotNull
    private String taskId;
    @NotNull
    private Project project;

    @NotNull
    private String projectId;

    @After
    public void after() throws AbstractException {
        taskService.clear();
    }

    @Before
    public void before() throws AbstractException {
        task = new Task();
        taskId = task.getId();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(userId);
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        taskRepository.add(task, userId);
        taskService = new TaskService(taskRepository);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        project = new Project();
        projectId = project.getId();
        project.setName("project");
        project.setDescription("project");
        projectRepository.add(project, userId);
        projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskByProject(projectId, taskId, userId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        @NotNull final Task newTask = taskService.add(userId, newTaskName, newTaskDescription);
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertEquals(newTask.getName(), newTaskName);
        Assert.assertEquals(newTask.getDescription(), newTaskDescription);
    }

    @Test
    public void findAllTasksByProjectId() throws AbstractException {
        bindTaskToProject();
        @NotNull final List<Task> tasks = projectTaskService.findAllTaskByProjectId(userId, projectId);
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(task, tasks.get(0));
    }

    @Test
    public void findByName() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final Task task = taskService.findByName(userId, taskName);
        Assert.assertEquals(task.getName(), taskName);
        Assert.assertEquals(task.getDescription(), taskDescription);
        Assert.assertEquals(task.getUserId(), userId);
    }

    @Test
    public void finishById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.completeById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(0, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.completeByIndex(0, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
    }

    @Test
    public void finishByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.completeByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startById(userId, taskId);
        Assert.assertNotNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(0, userId);
        Assert.assertNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByIndex(0, userId);
        Assert.assertNotNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByName(userId, taskName);
        Assert.assertNotNull(tempTask.getDateStart());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        bindTaskToProject();
        projectTaskService.unbindTaskFromProject(taskId, userId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateById(userId, taskId, newTaskName, newTaskDescription);
        Assert.assertEquals(task, taskService.findById(userId, taskId));
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateByIndex(0, userId, newTaskName, newTaskDescription);
        Assert.assertEquals(task, taskService.findByIndex(0, userId));
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.changeStatusById(taskId, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.changeStatusById(taskId, Status.COMPLETE, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
        taskService.changeStatusById(taskId, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(0, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.changeStatusByIndex(0, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.changeStatusByIndex(0, Status.COMPLETE, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
        taskService.changeStatusByIndex(0, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.changeStatusByName(taskName, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.changeStatusByName(taskName, Status.COMPLETE, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETE);
        taskService.changeStatusByName(taskName, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

}
