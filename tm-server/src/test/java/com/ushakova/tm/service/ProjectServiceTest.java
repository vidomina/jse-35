package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.service.IProjectService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectServiceTest {

    @NotNull
    private final String projectName = "projectName";
    @NotNull
    private final String projectDescription = "projectDescription";
    @NotNull
    private final String userId = "userId";
    @NotNull
    private IProjectService projectService;
    @NotNull
    private Project project;
    @NotNull
    private String projectId;

    @After
    public void after() throws AbstractException {
        projectService.clear();
    }

    @Before
    public void before() throws AbstractException {
        project = new Project();
        projectId = project.getId();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        projectRepository.add(project, userId);
        projectService = new ProjectService(projectRepository);
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        @NotNull final Project newProject = projectService.add(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertEquals(newProject.getName(), newProjectName);
        Assert.assertEquals(newProject.getDescription(), newProjectDescription);
    }

    @Test
    public void findByName() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final Project project = projectService.findByName(userId, projectName);
        Assert.assertEquals(project.getName(), projectName);
        Assert.assertEquals(project.getDescription(), projectDescription);
        Assert.assertEquals(project.getUserId(), userId);
    }

    @Test
    public void finishById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(0, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeByIndex(0, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void finishByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startById(userId, projectId);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(0, userId);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByIndex(0, userId);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByName(userId, projectName);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateByIndex(0, newProjectName, newProjectDescription, userId);
        Assert.assertEquals(project, projectService.findByIndex(0, userId));
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusById(projectId, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusById(projectId, Status.COMPLETE, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusById(projectId, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByIndex(0, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByIndex(0, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByIndex(0, Status.COMPLETE, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusByIndex(0, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByName(projectName, Status.IN_PROGRESS, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByName(projectName, Status.COMPLETE, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusByName(projectName, Status.NOT_STARTED, userId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

}
