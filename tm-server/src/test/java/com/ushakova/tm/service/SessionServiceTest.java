package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.ISessionRepository;
import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.IPropertyService;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.User;
import com.ushakova.tm.repository.SessionRepository;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userLogin";

    @NotNull
    private final String userPassword = "userPassword";

    @NotNull
    private Session session;

    public SessionServiceTest() throws AbstractException {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        user.setPasswordHash(HashUtil.salt(secret, iteration, userPassword));
        userService.add(user);
    }

    @Test
    public void close() {
        Assert.assertEquals(userId, session.getUserId());
        Assert.assertTrue(sessionService.close(session));
        Assert.assertFalse(sessionService.close(session));
    }

    @After
    public void finalizeTest() {
        sessionService.close(session);
    }

    @Before
    public void initializeTest() throws AbstractException {
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void validate() throws AbstractException {
        sessionService.validate(session);
    }

}
