package com.ushakova.tm.api.repository;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<String> getArguments();

    @Nullable
    AbstractCommand getCommandByArg(String arg);

    @Nullable
    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommandHasArgs();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<AbstractCommand> getCommands();

}
