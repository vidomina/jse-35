package com.ushakova.tm.api.service;

import com.ushakova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IEndpointLocator {

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    Session getSession();

    void setSession(Session session);

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
