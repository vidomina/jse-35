package com.ushakova.tm.api.service;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<String> getArguments();

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getCommandHasArgs();

    @NotNull
    Collection<String> getCommandNameList();

    @NotNull
    Collection<AbstractCommand> getCommands();

}
