package com.ushakova.tm.component;

import com.ushakova.tm.bootstrap.Bootstrap;
import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {

    @NotNull
    private static final String PATH = "./";

    private static final int INTERVAL = 3;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;


    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(bootstrap.getCommandService().getCommandHasArgs()
                .stream()
                .map(AbstractCommand::name)
                .collect(Collectors.toList()
                ));
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    private void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(o.getName()))
                .forEach(o -> {
                    final String name = o.getName();
                    bootstrap.parseCommand(name);
                    try {
                        o.delete();
                    } catch (Exception e) {
                        System.err.println(e.getMessage() + " Unable to delete file!");
                    }
                });
    }

}
