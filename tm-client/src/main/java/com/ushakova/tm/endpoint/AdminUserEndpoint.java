package com.ushakova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.3.0
 * 2021-07-06T00:48:48.354+03:00
 * Generated source version: 3.3.0
 */
@WebService(targetNamespace = "http://endpoint.tm.ushakova.com/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/addAllUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/addAllUserResponse")
    @RequestWrapper(localName = "addAllUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.AddAllUser")
    @ResponseWrapper(localName = "addAllUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.AddAllUserResponse")
    public void addAllUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "entities", targetNamespace = "")
                    java.util.List<com.ushakova.tm.endpoint.User> entities
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/addUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/addUserResponse")
    @RequestWrapper(localName = "addUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.AddUser")
    @ResponseWrapper(localName = "addUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.AddUserResponse")
    public void addUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "entity", targetNamespace = "")
                    com.ushakova.tm.endpoint.User entity
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/clearUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/clearUserResponse")
    @RequestWrapper(localName = "clearUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.ClearUser")
    @ResponseWrapper(localName = "clearUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.ClearUserResponse")
    public void clearUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserResponse")
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User createUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserWithEmailRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserWithEmailResponse")
    @RequestWrapper(localName = "createUserWithEmail", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUserWithEmail")
    @ResponseWrapper(localName = "createUserWithEmailResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUserWithEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User createUserWithEmail(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserWithRoleRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/createUserWithRoleResponse")
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.CreateUserWithRoleResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User createUserWithRole(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "role", targetNamespace = "")
                    com.ushakova.tm.endpoint.Role role
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findAllUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findAllUserResponse")
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.ushakova.tm.endpoint.User> findAllUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findUserByIdRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findUserByIdResponse")
    @RequestWrapper(localName = "findUserById", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindUserById")
    @ResponseWrapper(localName = "findUserByIdResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User findUserById(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findUserByLoginRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/findUserByLoginResponse")
    @RequestWrapper(localName = "findUserByLogin", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindUserByLogin")
    @ResponseWrapper(localName = "findUserByLoginResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.FindUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User findUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.LockUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User lockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserResponse")
    @RequestWrapper(localName = "removeUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUser")
    @ResponseWrapper(localName = "removeUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUserResponse")
    public void removeUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "entity", targetNamespace = "")
                    com.ushakova.tm.endpoint.User entity
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserByIdRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserByIdResponse")
    @RequestWrapper(localName = "removeUserById", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUserById")
    @ResponseWrapper(localName = "removeUserByIdResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUserByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User removeUserById(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserByLoginRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/removeUserByLoginResponse")
    @RequestWrapper(localName = "removeUserByLogin", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUserByLogin")
    @ResponseWrapper(localName = "removeUserByLoginResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.RemoveUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User removeUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/setPasswordUserRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/setPasswordUserResponse")
    @RequestWrapper(localName = "setPasswordUser", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.SetPasswordUser")
    @ResponseWrapper(localName = "setPasswordUserResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.SetPasswordUserResponse")
    public void setPasswordUser(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.UnlockUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.ushakova.tm.endpoint.User unlockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/updateUserByIdRequest", output = "http://endpoint.tm.ushakova.com/AdminUserEndpoint/updateUserByIdResponse")
    @RequestWrapper(localName = "updateUserById", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.UpdateUserById")
    @ResponseWrapper(localName = "updateUserByIdResponse", targetNamespace = "http://endpoint.tm.ushakova.com/", className = "com.ushakova.tm.endpoint.UpdateUserByIdResponse")
    public void updateUserById(
            @WebParam(name = "session", targetNamespace = "")
                    com.ushakova.tm.endpoint.Session session,
            @WebParam(name = "firstName", targetNamespace = "")
                    java.lang.String firstName,
            @WebParam(name = "lastName", targetNamespace = "")
                    java.lang.String lastName,
            @WebParam(name = "middleName", targetNamespace = "")
                    java.lang.String middleName
    );
}
