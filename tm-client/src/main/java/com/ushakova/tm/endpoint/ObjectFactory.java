package com.ushakova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.ushakova.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoadBackup_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadBackup");
    private final static QName _LoadBackupResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadBackupResponse");
    private final static QName _LoadDataBase64_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataBase64");
    private final static QName _LoadDataBase64Response_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataBase64Response");
    private final static QName _LoadDataBin_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataBin");
    private final static QName _LoadDataBinResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataBinResponse");
    private final static QName _LoadDataJson_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataJson");
    private final static QName _LoadDataJsonJaxB_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataJsonJaxB");
    private final static QName _LoadDataJsonJaxBResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataJsonJaxBResponse");
    private final static QName _LoadDataJsonResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataJsonResponse");
    private final static QName _LoadDataXml_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataXml");
    private final static QName _LoadDataXmlJaxB_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataXmlJaxB");
    private final static QName _LoadDataXmlJaxBResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataXmlJaxBResponse");
    private final static QName _LoadDataXmlResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataXmlResponse");
    private final static QName _LoadDataYaml_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataYaml");
    private final static QName _LoadDataYamlResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "loadDataYamlResponse");
    private final static QName _SaveBackup_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveBackup");
    private final static QName _SaveBackupResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveBackupResponse");
    private final static QName _SaveDataBase64_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataBase64");
    private final static QName _SaveDataBase64Response_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataBase64Response");
    private final static QName _SaveDataBin_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataBin");
    private final static QName _SaveDataBinResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataBinResponse");
    private final static QName _SaveDataJson_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataJson");
    private final static QName _SaveDataJsonJaxB_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataJsonJaxB");
    private final static QName _SaveDataJsonJaxBResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataJsonJaxBResponse");
    private final static QName _SaveDataJsonResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataJsonResponse");
    private final static QName _SaveDataXml_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataXml");
    private final static QName _SaveDataXmlJaxB_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataXmlJaxB");
    private final static QName _SaveDataXmlJaxBResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataXmlJaxBResponse");
    private final static QName _SaveDataXmlResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataXmlResponse");
    private final static QName _SaveDataYaml_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataYaml");
    private final static QName _SaveDataYamlResponse_QNAME = new QName("http://endpoint.tm.ushakova.com/", "saveDataYamlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ushakova.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoadBackup }
     */
    public LoadBackup createLoadBackup() {
        return new LoadBackup();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadBackup")
    public JAXBElement<LoadBackup> createLoadBackup(LoadBackup value) {
        return new JAXBElement<LoadBackup>(_LoadBackup_QNAME, LoadBackup.class, null, value);
    }

    /**
     * Create an instance of {@link LoadBackupResponse }
     */
    public LoadBackupResponse createLoadBackupResponse() {
        return new LoadBackupResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadBackupResponse")
    public JAXBElement<LoadBackupResponse> createLoadBackupResponse(LoadBackupResponse value) {
        return new JAXBElement<LoadBackupResponse>(_LoadBackupResponse_QNAME, LoadBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataBase64 }
     */
    public LoadDataBase64 createLoadDataBase64() {
        return new LoadDataBase64();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataBase64 }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataBase64")
    public JAXBElement<LoadDataBase64> createLoadDataBase64(LoadDataBase64 value) {
        return new JAXBElement<LoadDataBase64>(_LoadDataBase64_QNAME, LoadDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataBase64Response }
     */
    public LoadDataBase64Response createLoadDataBase64Response() {
        return new LoadDataBase64Response();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64Response }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataBase64Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataBase64Response")
    public JAXBElement<LoadDataBase64Response> createLoadDataBase64Response(LoadDataBase64Response value) {
        return new JAXBElement<LoadDataBase64Response>(_LoadDataBase64Response_QNAME, LoadDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataBin }
     */
    public LoadDataBin createLoadDataBin() {
        return new LoadDataBin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataBin")
    public JAXBElement<LoadDataBin> createLoadDataBin(LoadDataBin value) {
        return new JAXBElement<LoadDataBin>(_LoadDataBin_QNAME, LoadDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataBinResponse }
     */
    public LoadDataBinResponse createLoadDataBinResponse() {
        return new LoadDataBinResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataBinResponse")
    public JAXBElement<LoadDataBinResponse> createLoadDataBinResponse(LoadDataBinResponse value) {
        return new JAXBElement<LoadDataBinResponse>(_LoadDataBinResponse_QNAME, LoadDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataJson }
     */
    public LoadDataJson createLoadDataJson() {
        return new LoadDataJson();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJson }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataJson")
    public JAXBElement<LoadDataJson> createLoadDataJson(LoadDataJson value) {
        return new JAXBElement<LoadDataJson>(_LoadDataJson_QNAME, LoadDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataJsonJaxB }
     */
    public LoadDataJsonJaxB createLoadDataJsonJaxB() {
        return new LoadDataJsonJaxB();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxB }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataJsonJaxB")
    public JAXBElement<LoadDataJsonJaxB> createLoadDataJsonJaxB(LoadDataJsonJaxB value) {
        return new JAXBElement<LoadDataJsonJaxB>(_LoadDataJsonJaxB_QNAME, LoadDataJsonJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataJsonJaxBResponse }
     */
    public LoadDataJsonJaxBResponse createLoadDataJsonJaxBResponse() {
        return new LoadDataJsonJaxBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxBResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataJsonJaxBResponse")
    public JAXBElement<LoadDataJsonJaxBResponse> createLoadDataJsonJaxBResponse(LoadDataJsonJaxBResponse value) {
        return new JAXBElement<LoadDataJsonJaxBResponse>(_LoadDataJsonJaxBResponse_QNAME, LoadDataJsonJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataJsonResponse }
     */
    public LoadDataJsonResponse createLoadDataJsonResponse() {
        return new LoadDataJsonResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataJsonResponse")
    public JAXBElement<LoadDataJsonResponse> createLoadDataJsonResponse(LoadDataJsonResponse value) {
        return new JAXBElement<LoadDataJsonResponse>(_LoadDataJsonResponse_QNAME, LoadDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataXml }
     */
    public LoadDataXml createLoadDataXml() {
        return new LoadDataXml();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXml }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataXml")
    public JAXBElement<LoadDataXml> createLoadDataXml(LoadDataXml value) {
        return new JAXBElement<LoadDataXml>(_LoadDataXml_QNAME, LoadDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataXmlJaxB }
     */
    public LoadDataXmlJaxB createLoadDataXmlJaxB() {
        return new LoadDataXmlJaxB();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxB }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataXmlJaxB")
    public JAXBElement<LoadDataXmlJaxB> createLoadDataXmlJaxB(LoadDataXmlJaxB value) {
        return new JAXBElement<LoadDataXmlJaxB>(_LoadDataXmlJaxB_QNAME, LoadDataXmlJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataXmlJaxBResponse }
     */
    public LoadDataXmlJaxBResponse createLoadDataXmlJaxBResponse() {
        return new LoadDataXmlJaxBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxBResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataXmlJaxBResponse")
    public JAXBElement<LoadDataXmlJaxBResponse> createLoadDataXmlJaxBResponse(LoadDataXmlJaxBResponse value) {
        return new JAXBElement<LoadDataXmlJaxBResponse>(_LoadDataXmlJaxBResponse_QNAME, LoadDataXmlJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataXmlResponse }
     */
    public LoadDataXmlResponse createLoadDataXmlResponse() {
        return new LoadDataXmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataXmlResponse")
    public JAXBElement<LoadDataXmlResponse> createLoadDataXmlResponse(LoadDataXmlResponse value) {
        return new JAXBElement<LoadDataXmlResponse>(_LoadDataXmlResponse_QNAME, LoadDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataYaml }
     */
    public LoadDataYaml createLoadDataYaml() {
        return new LoadDataYaml();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataYaml }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataYaml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataYaml")
    public JAXBElement<LoadDataYaml> createLoadDataYaml(LoadDataYaml value) {
        return new JAXBElement<LoadDataYaml>(_LoadDataYaml_QNAME, LoadDataYaml.class, null, value);
    }

    /**
     * Create an instance of {@link LoadDataYamlResponse }
     */
    public LoadDataYamlResponse createLoadDataYamlResponse() {
        return new LoadDataYamlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataYamlResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LoadDataYamlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "loadDataYamlResponse")
    public JAXBElement<LoadDataYamlResponse> createLoadDataYamlResponse(LoadDataYamlResponse value) {
        return new JAXBElement<LoadDataYamlResponse>(_LoadDataYamlResponse_QNAME, LoadDataYamlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveBackup }
     */
    public SaveBackup createSaveBackup() {
        return new SaveBackup();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveBackup")
    public JAXBElement<SaveBackup> createSaveBackup(SaveBackup value) {
        return new JAXBElement<SaveBackup>(_SaveBackup_QNAME, SaveBackup.class, null, value);
    }

    /**
     * Create an instance of {@link SaveBackupResponse }
     */
    public SaveBackupResponse createSaveBackupResponse() {
        return new SaveBackupResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveBackupResponse")
    public JAXBElement<SaveBackupResponse> createSaveBackupResponse(SaveBackupResponse value) {
        return new JAXBElement<SaveBackupResponse>(_SaveBackupResponse_QNAME, SaveBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataBase64 }
     */
    public SaveDataBase64 createSaveDataBase64() {
        return new SaveDataBase64();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64 }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataBase64 }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataBase64")
    public JAXBElement<SaveDataBase64> createSaveDataBase64(SaveDataBase64 value) {
        return new JAXBElement<SaveDataBase64>(_SaveDataBase64_QNAME, SaveDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataBase64Response }
     */
    public SaveDataBase64Response createSaveDataBase64Response() {
        return new SaveDataBase64Response();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64Response }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataBase64Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataBase64Response")
    public JAXBElement<SaveDataBase64Response> createSaveDataBase64Response(SaveDataBase64Response value) {
        return new JAXBElement<SaveDataBase64Response>(_SaveDataBase64Response_QNAME, SaveDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataBin }
     */
    public SaveDataBin createSaveDataBin() {
        return new SaveDataBin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataBin")
    public JAXBElement<SaveDataBin> createSaveDataBin(SaveDataBin value) {
        return new JAXBElement<SaveDataBin>(_SaveDataBin_QNAME, SaveDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataBinResponse }
     */
    public SaveDataBinResponse createSaveDataBinResponse() {
        return new SaveDataBinResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataBinResponse")
    public JAXBElement<SaveDataBinResponse> createSaveDataBinResponse(SaveDataBinResponse value) {
        return new JAXBElement<SaveDataBinResponse>(_SaveDataBinResponse_QNAME, SaveDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataJson }
     */
    public SaveDataJson createSaveDataJson() {
        return new SaveDataJson();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJson }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataJson")
    public JAXBElement<SaveDataJson> createSaveDataJson(SaveDataJson value) {
        return new JAXBElement<SaveDataJson>(_SaveDataJson_QNAME, SaveDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataJsonJaxB }
     */
    public SaveDataJsonJaxB createSaveDataJsonJaxB() {
        return new SaveDataJsonJaxB();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxB }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataJsonJaxB")
    public JAXBElement<SaveDataJsonJaxB> createSaveDataJsonJaxB(SaveDataJsonJaxB value) {
        return new JAXBElement<SaveDataJsonJaxB>(_SaveDataJsonJaxB_QNAME, SaveDataJsonJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataJsonJaxBResponse }
     */
    public SaveDataJsonJaxBResponse createSaveDataJsonJaxBResponse() {
        return new SaveDataJsonJaxBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxBResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataJsonJaxBResponse")
    public JAXBElement<SaveDataJsonJaxBResponse> createSaveDataJsonJaxBResponse(SaveDataJsonJaxBResponse value) {
        return new JAXBElement<SaveDataJsonJaxBResponse>(_SaveDataJsonJaxBResponse_QNAME, SaveDataJsonJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataJsonResponse }
     */
    public SaveDataJsonResponse createSaveDataJsonResponse() {
        return new SaveDataJsonResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataJsonResponse")
    public JAXBElement<SaveDataJsonResponse> createSaveDataJsonResponse(SaveDataJsonResponse value) {
        return new JAXBElement<SaveDataJsonResponse>(_SaveDataJsonResponse_QNAME, SaveDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataXml }
     */
    public SaveDataXml createSaveDataXml() {
        return new SaveDataXml();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXml }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataXml")
    public JAXBElement<SaveDataXml> createSaveDataXml(SaveDataXml value) {
        return new JAXBElement<SaveDataXml>(_SaveDataXml_QNAME, SaveDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataXmlJaxB }
     */
    public SaveDataXmlJaxB createSaveDataXmlJaxB() {
        return new SaveDataXmlJaxB();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxB }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataXmlJaxB")
    public JAXBElement<SaveDataXmlJaxB> createSaveDataXmlJaxB(SaveDataXmlJaxB value) {
        return new JAXBElement<SaveDataXmlJaxB>(_SaveDataXmlJaxB_QNAME, SaveDataXmlJaxB.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataXmlJaxBResponse }
     */
    public SaveDataXmlJaxBResponse createSaveDataXmlJaxBResponse() {
        return new SaveDataXmlJaxBResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxBResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlJaxBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataXmlJaxBResponse")
    public JAXBElement<SaveDataXmlJaxBResponse> createSaveDataXmlJaxBResponse(SaveDataXmlJaxBResponse value) {
        return new JAXBElement<SaveDataXmlJaxBResponse>(_SaveDataXmlJaxBResponse_QNAME, SaveDataXmlJaxBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataXmlResponse }
     */
    public SaveDataXmlResponse createSaveDataXmlResponse() {
        return new SaveDataXmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataXmlResponse")
    public JAXBElement<SaveDataXmlResponse> createSaveDataXmlResponse(SaveDataXmlResponse value) {
        return new JAXBElement<SaveDataXmlResponse>(_SaveDataXmlResponse_QNAME, SaveDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataYaml }
     */
    public SaveDataYaml createSaveDataYaml() {
        return new SaveDataYaml();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataYaml }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataYaml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataYaml")
    public JAXBElement<SaveDataYaml> createSaveDataYaml(SaveDataYaml value) {
        return new JAXBElement<SaveDataYaml>(_SaveDataYaml_QNAME, SaveDataYaml.class, null, value);
    }

    /**
     * Create an instance of {@link SaveDataYamlResponse }
     */
    public SaveDataYamlResponse createSaveDataYamlResponse() {
        return new SaveDataYamlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataYamlResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SaveDataYamlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.ushakova.com/", name = "saveDataYamlResponse")
    public JAXBElement<SaveDataYamlResponse> createSaveDataYamlResponse(SaveDataYamlResponse value) {
        return new JAXBElement<SaveDataYamlResponse>(_SaveDataYamlResponse_QNAME, SaveDataYamlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

}
