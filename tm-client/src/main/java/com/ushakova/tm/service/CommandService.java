package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.api.service.ICommandService;
import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    @NotNull
    public Collection<String> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public @NotNull
    Collection<AbstractCommand> getCommandHasArgs() {
        return commandRepository.getCommandHasArgs();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNameList() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}