package com.ushakova.tm.util;

import com.ushakova.tm.api.other.ISaltSettings;
import com.ushakova.tm.exception.empty.EmptyPasswordException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull
    static String md5(@Nullable final String value) {
        if (value == null) throw new EmptyPasswordException();
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 1; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    static String salt(@NotNull final ISaltSettings setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(value, iteration, secret);
    }

    @NotNull
    static String salt(@Nullable final String value,
                       @NotNull final Integer iteration,
                       @NotNull final String secret
    ) {
        if (value == null) throw new EmptyPasswordException();
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

}
