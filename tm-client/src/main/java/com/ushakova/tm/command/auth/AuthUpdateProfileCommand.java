package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthUpdateProfileCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update user profile.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Update profile:");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        endpointLocator.getAdminUserEndpoint().updateUserById(session, firstName, lastName, middleName);
    }

    @Override
    @NotNull
    public String name() {
        return "user-update-profile";
    }

}
