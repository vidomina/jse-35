package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class AuthShowProfileInfoCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show user profile.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().findUserById(session, session.getUserId());
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("Show Profile:");
        showUserInfo(user);
    }

    @Override
    @NotNull
    public String name() {
        return "user-show-profile";
    }

}
