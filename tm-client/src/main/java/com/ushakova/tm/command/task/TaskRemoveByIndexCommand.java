package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove task by index.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Remove Task***\nEnter Index:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = endpointLocator.getTaskEndpoint().removeByIndex(session, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}