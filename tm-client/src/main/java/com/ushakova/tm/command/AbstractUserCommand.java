package com.ushakova.tm.command;

import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUserInfo(@Nullable final User user) {
        if (user == null) throw new TaskNotFoundException();
        System.out.println("Id: " + user.getId()
                + "\nFirst Name: " + user.getFirstName()
                + "\nMiddle Name: " + user.getMiddleName()
                + "\nLast Name: " + user.getLastName()
                + "\nEmail: " + user.getEmail()
                + "\nLogin: " + user.getLogin()
                + "\nRole: " + user.getRole().value());
    }

}
