package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthSetPasswordCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set user password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter user id:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSession();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().findUserById(session, userId);
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        endpointLocator.getAdminUserEndpoint().setPasswordUser(session, userId, password);
    }

    @Override
    @NotNull
    public String name() {
        return "user-set-password";
    }

}
