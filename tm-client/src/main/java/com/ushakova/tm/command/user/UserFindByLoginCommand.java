package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserFindByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find user by login.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Enter user login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().findUserByLogin(session, login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println(user);
    }

    @Override
    @NotNull
    public String name() {
        return "user-find-by-login";
    }

}
