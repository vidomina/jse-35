package com.ushakova.tm.command;

import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProjectInfo(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId()
                + "\nTitle: " + project.getName()
                + "\nDescription: " + project.getDescription()
                + "\nStatus: " + project.getStatus().value()
                + "\nStart Date: " + project.getDateStart()
                + "\nExpiration Date: " + project.getDateFinish()
                + "\nCreate Date: " + project.getDateCreate());
    }

}
