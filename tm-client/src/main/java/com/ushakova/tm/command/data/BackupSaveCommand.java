package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.endpoint.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BackupSaveCommand extends AbstractDataCommand {

    @Override
    public @Nullable
    String arg() {
        return null;
    }

    @Override
    public @Nullable
    String description() {
        return "Save backup data.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().saveBackup(session);
        System.out.println("Success!");
    }

    @Override
    public @NotNull
    String name() {
        return "bkp-save";
    }

}
