package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ExitCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

}
