package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskFindByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show task by id.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Show Task***\nEnter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().findById(session, id);
        showTaskInfo(task);
    }

    @Override
    @NotNull
    public String name() {
        return "task-view-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
