package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class UserShowListCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "List of users.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("User list:");
        @Nullable List<User> users = endpointLocator.getAdminUserEndpoint().findAllUser(session);
        Optional.ofNullable(users).orElseThrow(EntityNotFoundException::new);
        int index = 1;
        for (@NotNull final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Override
    @NotNull
    public String name() {
        return "user-list";
    }

}
