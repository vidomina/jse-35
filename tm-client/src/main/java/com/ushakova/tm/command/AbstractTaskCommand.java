package com.ushakova.tm.command;

import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTaskInfo(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId()
                + "\nTitle: " + task.getName()
                + "\nDescription: " + task.getDescription()
                + "\nStatus: " + task.getStatus().value()
                + "\nStart Date: " + task.getDateStart()
                + "\nExpiration Date: " + task.getDateFinish()
                + "\nCreate Date: " + task.getDateCreate());
    }

}
