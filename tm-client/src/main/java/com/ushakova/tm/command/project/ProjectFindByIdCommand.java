package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectFindByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        final @NotNull Session session = endpointLocator.getSession();
        System.out.println("***Show Project***\nEnter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        final Project project = endpointLocator.getProjectEndpoint().findProjectById(session, id);
        showProjectInfo(project);
    }

    @Override
    @NotNull
    public String name() {
        return "project-view-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
