package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteProjectByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"Complete\" status to project by id.";
    }

    public void execute() {
        final @NotNull Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().completeProjectById(session, id);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-project-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
