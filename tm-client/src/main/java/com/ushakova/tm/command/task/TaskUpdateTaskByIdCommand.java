package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskUpdateTaskByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update task by id.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Update Task***\nEnter Id:\"");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().findById(session, id);
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().updateById(session, id, name, description);
    }

    @Override
    @NotNull
    public String name() {
        return "task-update-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
