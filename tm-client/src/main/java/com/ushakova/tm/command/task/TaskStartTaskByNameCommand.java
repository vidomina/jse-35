package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskStartTaskByNameCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"In Progress\" status to task by id.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"In Progress\" to Task***\nEnter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().startByName(session, name);
    }

    @Override
    @NotNull
    public String name() {
        return "start-task-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
