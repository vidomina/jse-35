package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class ArgumentsListCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "show application arguments.";
    }

    @Override
    public void execute() {
        @Nullable final Collection<String> arguments = endpointLocator.getCommandService().getArguments();
        System.out.println("Available arguments:");
        for (final String argument : arguments) {
            System.out.println(argument);
        }
    }

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

}