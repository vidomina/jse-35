package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskStartTaskByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"In Progress\" status to task by index.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"In Progress\" to Task***\nEnter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = endpointLocator.getTaskEndpoint().startByIndex(session, index);
    }

    @Override
    @NotNull
    public String name() {
        return "start-task-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
