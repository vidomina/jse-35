package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.endpoint.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BackupLoadCommand extends AbstractDataCommand {

    @Override
    public @Nullable
    String arg() {
        return null;
    }

    @Override
    public @Nullable
    String description() {
        return "Load backup data.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().loadBackup(session);
        System.out.println("Success!");
    }

    @Override
    public @NotNull
    String name() {
        return "bkp-load";
    }

}
