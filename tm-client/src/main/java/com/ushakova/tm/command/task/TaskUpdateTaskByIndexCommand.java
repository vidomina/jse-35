package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskUpdateTaskByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update task by index.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Update Task***\nEnter Index:\n");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().updateByIndex(session, index, name, description);
    }

    @Override
    @NotNull
    public String name() {
        return "task-update-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
