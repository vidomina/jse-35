package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.User;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Remove User***");
        System.out.println("Enter User Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final User user = endpointLocator.getAdminUserEndpoint().removeUserByLogin(session, login);
        System.out.println("***Ok***");
    }

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

