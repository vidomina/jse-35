package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Load data from xml file by JAXB.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().loadDataJsonJaxB(session);
        System.out.println("Success!");
    }

    @Override
    @NotNull
    public String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

