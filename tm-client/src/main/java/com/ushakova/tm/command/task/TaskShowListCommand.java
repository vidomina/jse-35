package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.Task;
import com.ushakova.tm.enumerated.Sort;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show task list.";
    }

    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("***Task List***");
        System.out.println("***Enter Sort: ");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks = endpointLocator.getTaskEndpoint().findAll(session);
        int index = 1;
        Optional.ofNullable(tasks).orElseThrow(EntityNotFoundException::new);
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
