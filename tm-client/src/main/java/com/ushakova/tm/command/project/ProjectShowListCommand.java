package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.enumerated.Sort;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class ProjectShowListCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show project list.";
    }

    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("***Project List***");
        System.out.println("***Enter Sort: ");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        int index = 1;
        @Nullable List<Project> projects = endpointLocator.getProjectEndpoint().findAllProjects(session);
        if (projects != null)
            for (@Nullable final Project project : projects) {
                System.out.println(index + ". " + project);
                index++;
            }
    }

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
