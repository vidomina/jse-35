package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectStartProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"In Progress\" status to project by index.";
    }

    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = endpointLocator.getProjectEndpoint().startProjectByIndex(session, index);
    }

    @Override
    @NotNull
    public String name() {
        return "start-project-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
