package com.ushakova.tm.exception.entity;

import com.ushakova.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("An error has occurred: entity not found.");
    }

}
