package com.ushakova.tm.exception.entity;

import com.ushakova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("An error has occurred: project not found.");
    }

}
