package com.ushakova.tm;

import com.ushakova.tm.endpoint.*;
import com.ushakova.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UserEndpointTest {

    @NotNull
    final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NotNull
    final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    @NotNull
    private final SessionEndpoint sessionEndpoint;
    @NotNull
    private final UserEndpoint userEndpoint;
    @NotNull
    final private String userLogin = "Guest";
    @NotNull
    final private String userEmail = "Guest@mail.ru";
    @NotNull
    private Session session;

    public UserEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("Guest", "Guest");
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setPasswordUser() {
        @NotNull final String oldPassword = adminUserEndpoint.findUserByLogin(session, "Guest").getPasswordHash();
        userEndpoint.updateUserPassword(session, "password");
        @NotNull final String newPassword = adminUserEndpoint.findUserByLogin(session, "Guest").getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(session);
        @NotNull final Session tempSession = sessionEndpoint.openSession("Admin", "Admin");
        adminUserEndpoint.removeUserByLogin(tempSession, userLogin);
        Assert.assertFalse(sessionEndpoint.closeSession(tempSession));
    }

}
