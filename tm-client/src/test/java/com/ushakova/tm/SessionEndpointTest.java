package com.ushakova.tm;

import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.endpoint.SessionEndpoint;
import com.ushakova.tm.endpoint.SessionEndpointService;
import com.ushakova.tm.exception.AbstractException;
import com.ushakova.tm.marker.SoapCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openCloseSession() throws AbstractException {
        @NotNull final Session sessionTemp = sessionEndpoint.openSession("Guest", "Guest");
        Assert.assertNotNull(sessionTemp);
        Assert.assertFalse(sessionEndpoint.closeSession(sessionTemp));
    }

}
