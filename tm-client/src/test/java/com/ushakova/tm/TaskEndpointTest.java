package com.ushakova.tm;

import com.ushakova.tm.endpoint.*;
import com.ushakova.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class TaskEndpointTest {

    @NotNull
    private final String taskName = "Task_35";
    @NotNull
    private SessionEndpoint sessionEndpoint;
    @NotNull
    private TaskEndpoint taskEndpoint;
    @NotNull
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private String taskId;

    @NotNull
    private Task task;

    @Nullable
    private String projectId;

    @NotNull
    private Session session;

    @After
    @SneakyThrows
    public void after() {
        if (taskId != null) removeTask(taskId);
        if (projectId != null) removeProject(projectId);
        sessionEndpoint.closeSession(session);
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        session = sessionEndpoint.openSession("user", "user");
        taskEndpoint.create(session, taskName, "taskDescription");
        taskId = taskEndpoint.findById(session, taskName).getId();
        projectEndpoint.createProject(session, "project", "project");
        projectId = projectEndpoint.findProjectByName(session, "Name").getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void bindTaskToProject() {
        Assert.assertNull(task.getProjectId());
        taskEndpoint.bindTaskToProject(session, projectId, taskId);
        task = taskEndpoint.findById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createTask() {
        taskEndpoint.create(session, "Task_35", "Hi");
        @NotNull final String tempTaskId = taskEndpoint.findByName(session, "Task_35").getId();
        Assert.assertNotNull(tempTaskId);
        Assert.assertNotNull(taskEndpoint.findById(session, tempTaskId));
        removeTask(tempTaskId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllTask() {
        Assert.assertEquals(1, taskEndpoint.findAll(session));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findTask() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findById(session, taskId));
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findByName(session, taskName));
        Assert.assertNotNull(taskEndpoint.findByIndex(session, 0));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void finishByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.completeById(session, taskId);
        Assert.assertEquals(taskEndpoint.findById(session, taskId).getStatus(), Status.COMPLETE);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdTask() {
        Assert.assertNotNull(taskId);
        removeTask(taskId);
        Assert.assertFalse(
                taskEndpoint
                        .findAll(session)
                        .stream()
                        .anyMatch(p -> p.getId().equals(task.getId()))
        );
        taskId = null;
    }

    @SneakyThrows
    private void removeProject(@NotNull final String id) {
        projectEndpoint.removeProjectById(session, id);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeProjectById() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskEndpoint.bindTaskToProject(session, projectId, taskId);
        removeProject(projectId);
        projectId = null;
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        Assert.assertNull(taskEndpoint.findById(session, taskId).getProjectId());
    }

    @SneakyThrows
    private void removeTask(@NotNull final String id) {
        taskEndpoint.removeById(session, id);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void startByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.startById(session, taskId);
        Assert.assertEquals(taskEndpoint.findById(session, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unbindTaskFromProject() {
        Assert.assertNull(task.getProjectId());
        taskEndpoint.bindTaskToProject(session, projectId, taskId);
        task = taskEndpoint.findById(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
        taskEndpoint.unbindTaskFromProject(session, taskId);
        Assert.assertNull(taskEndpoint.findById(session, taskId).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateByIdTask() {
        @NotNull final String newTaskName = "Task Name";
        @NotNull final String newTaskDescription = "Task Name";
        taskEndpoint.updateById(session, taskId, newTaskName, newTaskDescription);
        @NotNull final Task updatedTask = taskEndpoint.findById(session, taskId);
        Assert.assertEquals(task.getId(), updatedTask.getId());
        Assert.assertNotEquals(task.getName(), updatedTask.getName());
        Assert.assertNotEquals(task.getDescription(), updatedTask.getDescription());
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateStatusByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.changeStatusById(session, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskEndpoint.findById(session, taskId).getStatus(), Status.IN_PROGRESS);
        taskEndpoint.changeStatusById(session, taskId, Status.COMPLETE);
        Assert.assertEquals(taskEndpoint.findById(session, taskId).getStatus(), Status.COMPLETE);
    }

}
