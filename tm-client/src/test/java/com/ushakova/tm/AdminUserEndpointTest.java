package com.ushakova.tm;

import com.ushakova.tm.endpoint.*;
import com.ushakova.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AdminUserEndpointTest {

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String userLogin = "Guest";

    @NotNull
    private Session session;

    @Nullable
    private String userId;

    @NotNull
    private User user;

    @SneakyThrows
    public AdminUserEndpointTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @After
    @SneakyThrows
    public void after() {
        if (userId != null) adminUserEndpoint.removeUserById(session, userId);
        sessionEndpoint.closeSession(session);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createUser() {
        @NotNull final String tempUserLogin = "test_35";
        Assert.assertNotNull(adminUserEndpoint.createUser(session, tempUserLogin, "test_35"));
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(session, tempUserLogin));
        adminUserEndpoint.removeUserByLogin(session, tempUserLogin);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllUser() {
        Assert.assertFalse(adminUserEndpoint.findAllUser(session).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByIdUser() {
        @NotNull final User tempUser = adminUserEndpoint.findUserById(session, userId);
        Assert.assertNotNull(tempUser);
        Assert.assertEquals(userId, tempUser.getId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByLoginUser() {
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(session, userLogin));
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.openSession("Admin", "Admin");
        user = adminUserEndpoint.createUser(session, userLogin, "temp");
        userId = user.getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdUser() {
        Assert.assertTrue(userFind());
        adminUserEndpoint.removeUserById(session, userId);
        userId = null;
        Assert.assertFalse(userFind());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByLoginUser() {
        Assert.assertTrue(userFind());
        adminUserEndpoint.removeUserByLogin(session, userLogin);
        userId = null;
        Assert.assertFalse(userFind());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setRoleUser() {
        Assert.assertEquals(user.getRole(), Role.USER);
        user.setRole(Role.ADMIN);
        Assert.assertEquals(adminUserEndpoint.findUserById(session, userId).getRole(), Role.ADMIN);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unlockLockByLoginUser() {
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, userLogin);
        Assert.assertTrue(adminUserEndpoint.findUserById(session, userId).isLocked());
        adminUserEndpoint.unlockUserByLogin(session, userLogin);
        Assert.assertFalse(adminUserEndpoint.findUserById(session, userId).isLocked());
    }

    @SneakyThrows
    private boolean userFind() {
        return adminUserEndpoint
                .findAllUser(session)
                .stream()
                .anyMatch(u -> u.getId().equals(userId));
    }

}
